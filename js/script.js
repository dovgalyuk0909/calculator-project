class Main {
	constructor(navigation, cart) {
		this.$butResult = document.querySelector('.but-result');
		this.$butClearInputs = document.querySelector('.but-clear-inputs');
		this.$inputFalse = document.querySelector('.dont-check');
		this.$inputs = document.querySelectorAll('.fild');
		this.$fildResCountArea = document.querySelector('.count-area');
		this.$fildResCountMash = document.querySelector('.count-mash-tiles');

		this.$butResult.addEventListener('click', this.validate.bind(this));
		this.$butClearInputs.addEventListener('click', this.clearAll.bind(this));
	}

	validate() {//validate
		for(let fild of this.$inputs) {
			const valueInput = +fild.value;
			if(isNaN(valueInput)) {
				this.$inputFalse.innerText = 'Данные символы недопустимы. Импользуйте цыфры';
				this.$inputFalse.style.display = 'block';
				return
			} 
			else if (valueInput === 0) {
				this.$inputFalse.innerText = 'Заполните все поля, отмеченные звездочкой';
				this.$inputFalse.style.display = 'block';
				return;
			}
		}
		this.$inputFalse.style.display = 'none';
		this.operation();
	}
	clearAll() {
		for(let fild of this.$inputs) {
			fild.value = '';
		}
	}
	operation() {
		const $lengthArea = +document.querySelector('.length-tile').value;
		const $widthTile = +document.querySelector('.width-tile').value;
		const $tileThickness = +document.querySelector('.tile-thickness').value;
		const $widthSeam = +document.querySelector('.width-seam').value;
		const $area = +document.querySelector('.area').value;


		let resTileMash = (($lengthArea + $widthTile)/($lengthArea * $widthTile)) * $tileThickness * $widthSeam * 1.6;
		resTileMash = +resTileMash.toFixed(2);
		let resultArea = +($area*resTileMash).toFixed(1);
		//resultArea = +resultArea.toFixed(1);

		this.drawAmountResult(resultArea,resTileMash,$area); 
	}
	drawAmountResult(area,tiles,countArea){
		this.$fildResCountMash.innerHTML = `Расход затирки составит: ${tiles} кг/м<sup>2</sup>.`;
		if(area !== 0) {
			this.$fildResCountArea.innerHTML = `Необходимое количество затирки: ${area}кг. при площади ${countArea} м<sup>2</sup>.`;
		}
	}
}
document.addEventListener('DOMContentLoaded',()=> {
	new Main();
});
